package com.Arrays;

public class EmployeeRunner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double empSalary[]= {23500.0, 25080.0, 28760.0, 22340.0, 19890.0};
        int i,j,belowAvg=0,aboveAvg=0,eqlAvg=0;
        double sum=0,avg=0;
        
      EmployeeRecord emprec = new EmployeeRecord();
      
      emprec.setSalary(empSalary);
      
      
      for(i=0;i<5;i++) {
    	  sum=sum+emprec.getSalary()[i];
      }
      
      avg=sum/i;
      System.out.println("Average of five employees is "+avg);
      
      
      
      for(j=0;j<5;j++) {
    	  
    	  if(empSalary[j]<avg) {
    		  
    		  belowAvg=belowAvg+1;
    		  
    	  }
    	  else if(empSalary[j]>avg) {
    		 
    		  aboveAvg=aboveAvg+1; 
    	  
    	  }
    	  else if(empSalary[j]==avg) {
    		  
    		  eqlAvg=eqlAvg+1;
    	  
    	  }
      }
      
      System.out.println("Number of employees below average salary are "+belowAvg);
      System.out.println("Number of employees above average salary are "+aboveAvg);
      System.out.println("Number of employees equal to average salary are "+eqlAvg);
	
	}

}
